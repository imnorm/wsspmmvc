﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using WssPmCommon2.Models;

namespace WssPmDataAccess
{
    public class WssPmSqlDataInitializer : DropCreateDatabaseAlways<WssPmSqlData>
    {
        protected override void Seed(WssPmSqlData context)
        {
            var personSeedData = new List<Person>();

            personSeedData.Add(new Person(RoleType.BusinessAnalyst)
            {
                FirstName = "Ken",
                LastName = "Lay",
                Email = "klay@webstaurantstore.com",
                IsActive = true
            });
            personSeedData.Add(new Person(RoleType.BusinessAnalyst)
            {
                FirstName = "Danielle",
                LastName = "Benton",
                Email = "dbenton@webstaurantstore.com",
                IsActive = true
            });
            personSeedData.Add(new Person(RoleType.BusinessAnalyst)
            {
                FirstName = "Joey",
                LastName = "McWilliams",
                Email = "jmcwilliams@webstaurantstore.com",
                IsActive = true
            });
            personSeedData.Add(new Person(RoleType.BusinessAnalyst)
            {
                FirstName = "Brian",
                LastName = "Montgomery",
                Email = "bmontgomery@webstaurantstore.com",
                IsActive = true
            });
            personSeedData.Add(new Person(RoleType.BusinessAnalyst)
            {
                FirstName = "Angie",
                LastName = "Kissinger",
                Email = "akissinger@webstaurantstore.com",
                IsActive = true
            });
            personSeedData.Add(new Person(RoleType.BusinessAnalyst)
            {
                FirstName = "Dena",
                LastName = "Maounis",
                Email = "dmaounis@webstaurantstore.com",
                IsActive = true
            });
            personSeedData.Add(new Person(RoleType.BusinessAnalyst)
            {
                FirstName = "Scott",
                LastName = "Musser",
                Email = "smusser@webstaurantstore.com",
                IsActive = true
            });
            personSeedData.Add(new Person(RoleType.DevelopmentManager)
            {
                FirstName = "Nate",
                LastName = "Wagner",
                Email = "nwagner@webstaurantstore.com",
                IsActive = true
            });

            context.People.AddRange(personSeedData);

            base.Seed(context);
        }
    }

    public class WssPmSqlData : DbContext
    {
        public WssPmSqlData(string connectionName) : base("name=" + connectionName)
        {
            Database.SetInitializer(new WssPmSqlDataInitializer());
        }

        public DbSet<Person> People { get; set; }
        public DbSet<Project> Projects { get; set; }

        #region singleton implementation

        private static WssPmSqlData _instance;

        public static WssPmSqlData Instance
        {
            get
            {
                if (_instance == null)
                {
                    var machineName = Environment.MachineName;
                    var connectStrings = ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>();
                    if (!connectStrings.Any(n => n.Name.Equals(machineName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        machineName = "WssPmSql";
                    }

                    _instance = new WssPmSqlData(machineName);
                }
                return _instance;
            }
        }

        #endregion singleton implementation
    }
}