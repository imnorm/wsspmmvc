﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WssPmCommon2.Models;
using WssPmDataAccess;

namespace WssPmMVC2.Controllers
{
    public class ProjectController : Controller
    {
        public ActionResult Index(List<Project> projectList = null)
        {
            //if (projectList == null || projectList.Count == 0) projectList = Project.SampleData();
            if (projectList == null || projectList.Count == 0) projectList = WssPmSqlData.Instance.Projects.ToList();
            //ViewData["ProjectList"] = Models.Project.SampleData();
            return View(projectList);
        }

        [HttpPost]
        public ActionResult Add(Project project)
        {
            WssPmSqlData.Instance.Projects.Add(project);
            WssPmSqlData.Instance.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Add()
        {
            return View();
        }

        public ActionResult Edit(int projectId)
        {
            var matchingProduct = Project.SampleData().FirstOrDefault(p => p.Id == projectId);

            return View(matchingProduct);
        }
    }
}
