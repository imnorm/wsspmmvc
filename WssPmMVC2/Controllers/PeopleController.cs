﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WssPmCommon2.Models;
using WssPmDataAccess;

namespace WssPmMVC2.Controllers
{
    public class PeopleController : Controller
    {
        //QualityAnalyst = 0x01,
        //BusinessAnalyst = 0x02,
        //DevelopmentLead = 0x04,
        //DevelopmentManager = 0x08,
        //ProductOwner = 0x10,
        //AssistanProductOwner = 0x20

        // GET: People
        public ActionResult Index()
        {
            var allPeople = WssPmSqlData.Instance.People.ToList();
            return View(allPeople);
        }

        public ActionResult IndexByRole(string roleName)
        {
            var role = (RoleType)Enum.Parse(typeof(RoleType), roleName);

            ViewBag.Role = roleName;
            ViewBag.RoleName = role.AsString();
            ViewBag.People = WssPmSqlData.Instance.People.ToList().Where(p => p.HasRole(role)).ToList();

            return View();
        }


        [HttpPost]
        public ActionResult Add(Person person)
        {
            WssPmSqlData.Instance.People.Add(person);
            WssPmSqlData.Instance.SaveChanges();

            return RedirectToAction("IndexByRole", new { roleName = person.RoleType().ToString() });
        }

        public ActionResult Add(string roleName)
        {
            var role = (RoleType)Enum.Parse(typeof(RoleType), roleName);
            ViewBag.Role = roleName;
            var person = new Person(role);

            return View(person);
        }

        public ActionResult Edit(int personId)
        {
            return View(personId);
        }

        [HttpPost]
        public ActionResult Edit(Person person)
        {
            var matchedPerson = WssPmSqlData.Instance.People.FirstOrDefault(p => p.Id == person.Id);
            if (matchedPerson != null)
            {
                matchedPerson.IsActive = person.IsActive;
                matchedPerson.LastName = person.LastName;
                matchedPerson.FirstName = person.FirstName;
                matchedPerson.Email = person.Email;
                matchedPerson.ContactType = person.ContactType;
            }
            WssPmSqlData.Instance.SaveChanges();

            return RedirectToAction("Index");
        }

    }
}