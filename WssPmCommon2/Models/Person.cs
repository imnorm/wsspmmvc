﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WssPmCommon2.Models
{
    [Flags]
    public enum RoleType
    {
        QualityAnalyst = 0x01,
        BusinessAnalyst = 0x02,
        DevelopmentLead = 0x04,
        DevelopmentManager = 0x08,
        ProductOwner = 0x10,
        AssistanProductOwner = 0x20
    }

    public class Person
    {
        public int ContactType { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

        public Person()
        {

        }

        public Person(RoleType contactType)
        {
            ContactType = (int)contactType;
        }
        public RoleType RoleType()
        {
            return Roles().First();
        }

        public void AddRole(RoleType role)
        {
            if ((ContactType & (int)role) != (int)role) ContactType = ContactType + (int)role;
        }

        public void RemoveRole(RoleType role)
        {
            if ((ContactType & (int)role) == (int)role) ContactType -= (int)role;
        }

        public List<RoleType> Roles()
        {
            var roles = new List<RoleType>();
            var validRoles = Enum.GetValues(typeof(RoleType)).Cast<RoleType>().ToList();
            foreach (var r in validRoles)
            {
                if (HasRole(r))
                {
                    roles.Add(r);
                }
            }
            return roles;
        }

        public string AllRoles()
        {

            return string.Join(", ", Roles().Select(r => r.AsString()));
        }

        public bool HasRole(RoleType role)
        {
            return (ContactType & (int)role) == (int)role;
        }
    }

    //public class QualityAnalyst : Person
    //{
    //    public QualityAnalyst() : base(RoleType.QualityAnalyst)
    //    {

    //    }
    //}

    //public class BusinessAnalyst : Person
    //{
    //    public BusinessAnalyst() : base(RoleType.BusinessAnalyst)
    //    { }
    //}

    //public class DevelopmentLead : Person
    //{
    //    public DevelopmentLead() : base(RoleType.DevelopmentLead)
    //    {
    //    }
    //}

    //public class DevelopmentManager : Person
    //{
    //    public DevelopmentManager() : base(RoleType.DevelopmentManager)
    //    {
    //    }
    //}

    //public class ProductOwner : Person
    //{
    //    public ProductOwner() : base(RoleType.ProductOwner)
    //    {
    //    }
    //}

    //public class AssistanProductOwner : Person
    //{
    //    public AssistanProductOwner() : base(RoleType.AssistanProductOwner)
    //    {
    //    }
    //}



}
