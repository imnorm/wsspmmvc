﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WssPmCommon2.Models
{
    public static class RoleHelper
    {
        public static string AsString(this RoleType role)
        {

            var roleAsString = role.ToString();
            var resultString = string.Empty + roleAsString[0];
            for (var i = 1; i < roleAsString.Length; i++)
            {
                var c = roleAsString[i];
                if (Char.IsUpper(c)) resultString = resultString + " ";
                resultString = resultString + c;
            }
            return resultString.Trim();
        }
    }
}
