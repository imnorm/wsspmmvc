﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WssPmCommon2.Models
{
    public class Project
    {
        [DisplayName("ID")]
        public int Id { get; set; }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [DisplayName("Deadline")]
        public DateTime? Deadline { get; set; }

        [DisplayName("ETA")]
        public DateTime? EstimatedDone { get; set; }

        [DisplayName("Active")]
        public bool IsActive { get; set; }

        public List<Person> TeamMembers { get; set; }

        public Project()
        {

        }
        public Project(int id, string name, string description, DateTime? startDate, DateTime? deadline, DateTime? eta, bool isActive)
        {
            Id = id;
            Name = name;
            Description = description;
            StartDate = startDate;
            Deadline = deadline;
            EstimatedDone = eta;
            IsActive = isActive;
        }
        public static List<Project> SampleData()
        {
            return new List<Project>()
            {
                new Project(1, "Project A", "A Simple Project to put A's in the database",new DateTime(2018,11,1),new DateTime(2018,12,31),new DateTime(2018,12,16),true),
                new Project(2, "Project B", "A Simple Project to put B's in the database",new DateTime(2019,1,1),new DateTime(2019,2,28),new DateTime(2019,3,16),true),
                new Project(3, "Project C", "A Simple Project to put C's in the database",new DateTime(2019,3,1),new DateTime(2019,4,3),new DateTime(2019,2,16),true),
                new Project(4, "Project D", "A Simple Project to put D's in the database",new DateTime(2019,5,1),null,new DateTime(2019,6,16),false),
                new Project(5, "Project E", "A Simple Project to put E's in the database",new DateTime(2019,7,1),new DateTime(2019,8,30),new DateTime(2019,5,16),true),
                new Project(6, "Project F", "A Simple Project to put F's in the database",new DateTime(2019,9,1),new DateTime(2019,10,30),new DateTime(2019,11,16),true),
            };
        }

    }
}
