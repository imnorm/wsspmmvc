﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WssPmCommon2
{
    public static class Extensions
    {
        public static string ToString(this DateTime? source, string format)
        {
            if (!source.HasValue) return string.Empty;

            return source.Value.ToString(format);
        }

    }


}
