﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WssPmMVC.Controllers
{
    public class ProjectController : Controller
    {
        public IActionResult Index(List<Models.Project> projectList = null)
        {
            //if (projectList == null || projectList.Count == 0) projectList = Models.Project.SampleData();
            if (projectList == null || projectList.Count == 0) projectList = WssPmMVC.DataAccess.WssPmSqlData.Instance.Projects.ToList();
            //ViewData["ProjectList"] = Models.Project.SampleData();
            return View(projectList);
        }

        public IActionResult Add()
        {
            return View();
        }

        public IActionResult Edit(int projectId)
        {
            var matchingProduct = Models.Project.SampleData().FirstOrDefault(p => p.Id == projectId);

            return View(matchingProduct);
        }
    }
}