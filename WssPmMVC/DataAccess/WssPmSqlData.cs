﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using WssPmMVC.Models;

namespace WssPmMVC.DataAccess
{
    public class WssPmSqlData : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Project> Projects { get; set; }

        #region singleton implementation

        private static WssPmSqlData _instance;

        public static WssPmSqlData Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WssPmSqlData();
                }
                return _instance;
            }
        }

        #endregion


    }
}
