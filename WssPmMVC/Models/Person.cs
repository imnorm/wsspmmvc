﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WssPmMVC.Models
{
    [Flags]
    public enum RoleType
    {
        QualityAnalyst = 0x01,
        BusinessAnalyst = 0x02,
        DevelopmentLead = 0x04,
        DevelopmentManager = 0x08,
        ProductOwner = 0x10,
        AssistanProductOwner = 0x20
    }

    public abstract class Person
    {
        public RoleType ContactType { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        protected Person(RoleType contactType)
        {

        }
        public void AddRole(RoleType role)
        {
            if ((ContactType & role) != role) ContactType = ContactType | role;
        }

        public void RemoveRole(RoleType role)
        {
            if ((ContactType & role) == role) ContactType = ContactType & ~role;
        }

    }

    public class QualityAnalyst : Person
    {
        public QualityAnalyst() : base(RoleType.QualityAnalyst)
        {

        }
    }

    public class BusinessAnalyst : Person
    {
        public BusinessAnalyst() : base(RoleType.BusinessAnalyst)
        { }
    }

    public class DevelopmentLead : Person
    {
        public DevelopmentLead() : base(RoleType.DevelopmentLead)
        {
        }
    }

    public class DevelopmentManager : Person
    {
        public DevelopmentManager() : base(RoleType.DevelopmentManager)
        {
        }
    }

    public class ProductOwner : Person
    {
        public ProductOwner() : base(RoleType.ProductOwner)
        {
        }
    }

    public class AssistanProductOwner : Person
    {
        public AssistanProductOwner() : base(RoleType.AssistanProductOwner)
        {
        }
    }



}
